#!/usr/bin/env python3

from kivy.app import App
from kivy.core.window import Window
from kivy.uix.boxlayout import BoxLayout
from PrinterComm import PrinterComm
from math import exp, log

comm = None

laserPanel = None
fanPanel = None
jogPanel = None
homePanel = None
selectedPanel = None


class TitleBar(BoxLayout):
    pass


class BaseFunctionPanel(BoxLayout):
    def __init__(self, **kwargs):
        super(BaseFunctionPanel, self).__init__(**kwargs)
        self.name = 'NoFunction'
        self.status = '?'
        self.selected_now = False
        self.selected_prefix = '[color=11bbee]'
        self.selected_suffix = '[/color]'

    def select(self):
        self.selected_now = True
        self.ids.name_label.text = self.selected_prefix + self.name + self.selected_suffix

    def deselect(self):
        self.selected_now = False
        self.ids.name_label.text = self.name


class LaserPanel(BaseFunctionPanel):
    def __init__(self, **kwargs):
        super(LaserPanel, self).__init__(**kwargs)
        self.name = 'Laser'
        self.status = 'PWM: ?'
        self.current_pwm = None
        global laserPanel
        laserPanel = self

    def update_pwm(self, pwm):
        self.current_pwm = pwm
        if pwm >= 0:
            self.status = 'PWM: {}'.format(pwm)
        else:
            self.status = 'PWM: ?'
        comm.set_laser(pwm)
        self.ids.status_label.text = self.status

    def on_button_off(self):
        self.update_pwm(0)

    def on_button_low(self):
        self.update_pwm(2)

    def on_button_on(self):
        self.update_pwm(255)

    def on_button_prepare(self):
        print('Preparing laser')


class FanPanel(BaseFunctionPanel):
    def __init__(self, **kwargs):
        super(FanPanel, self).__init__(**kwargs)
        self.name = 'Fan'
        self.status = 'PWM: ?'
        self.current_pwm = None
        self.custom_pwm = 127
        global fanPanel
        fanPanel = self

    def on_custom_slider_value(self):
        self.custom_pwm = int(self.ids.custom_slider.value)
        self.ids.custom_button.text = format(self.custom_pwm / 2.55, '.0f') + '%'

    def update_pwm(self, pwm):
        self.current_pwm = pwm
        if pwm >= 0:
            self.status = 'PWM: {}'.format(pwm)
        else:
            self.status = 'PWM: ?'
        comm.set_fan(pwm)
        # TODO: Only update if successfully set
        self.ids.status_label.text = self.status

    def on_button_off(self):
        self.update_pwm(0)

    def on_button_custom(self):
        self.update_pwm(self.custom_pwm)

    def on_button_on(self):
        self.update_pwm(255)


class HomePanel(BaseFunctionPanel):
    def __init__(self, **kwargs):
        super(HomePanel, self).__init__(**kwargs)
        self.name = 'Home'
        self.status = ''
        global homePanel
        homePanel = self

    def on_button_all(self):
        comm.home('')

    def on_button_x(self):
        comm.home('X')

    def on_button_y(self):
        comm.home('Y')

    def on_button_z(self):
        comm.home('Z')

    def on_button_disable_steppers(self):
        comm.disable_steppers()


class JogPanel(BaseFunctionPanel):
    def __init__(self, **kwargs):
        super(JogPanel, self).__init__(**kwargs)
        self.name = 'Jog'
        self.status = '1.0'
        self.step = 1.0
        self.rate = 3000.0
        global jogPanel
        jogPanel = self

        self.max_rate = 3000.0
        self.exp_scale = self.max_rate / (exp(1.0) - 1.0)
        self.start_value = log(self.rate / self.exp_scale + 1)

    def update_status(self):
        self.status = format(self.step, '0.1f') + 'mm, ' + format(self.rate, '0.1f') + 'mm/min'
        self.ids.status_label.size_hint = (1.33, 1)
        self.ids.status_label.text = self.status

    def on_custom_slider_value(self):
        x = float(self.ids.custom_slider.value_normalized)
        self.rate = (exp(x) - 1.0) * self.exp_scale
        self.update_status()

    def on_button_step_small(self):
        self.step = 0.1
        self.update_status()

    def on_button_step_medium(self):
        self.step = 1.0
        self.update_status()

    def on_button_step_large(self):
        self.step = 10.0
        self.update_status()

    def on_button_x_neg(self):
        comm.jog('X', -self.step, self.rate)

    def on_button_x_pos(self):
        comm.jog('X', self.step, self.rate)

    def on_button_y_neg(self):
        comm.jog('Y', -self.step, self.rate)

    def on_button_y_pos(self):
        comm.jog('Y', self.step, self.rate)

    def on_button_z_neg(self):
        comm.jog('Z', -self.step, self.rate)

    def on_button_z_pos(self):
        comm.jog('Z', self.step, self.rate)


class MainScreen(BoxLayout):
    def __init__(self, **kwargs):
        super(MainScreen, self).__init__(**kwargs)
        Window.bind(on_key_down=self.press)

    def selectPanel(self, panel: BaseFunctionPanel):
        global selectedPanel
        if selectedPanel:
            selectedPanel.deselect()

        panel.select()
        selectedPanel = panel

    def press(self, window, key, scancode, codepoint, modifier):

        if key == 108:  # L key
            if len(modifier) == 0:
                self.selectPanel(laserPanel)
            elif modifier == ['shift']:
                laserPanel.on_button_on()
            elif modifier == ['ctrl']:
                laserPanel.on_button_off()
            elif modifier == ['shift', 'ctrl']:
                laserPanel.on_button_low()

        if key == 102:  # F key
            if len(modifier) == 0:
                self.selectPanel(fanPanel)
            elif modifier == ['shift']:
                fanPanel.on_button_on()
            elif modifier == ['ctrl']:
                fanPanel.on_button_off()
            elif modifier == ['shift', 'ctrl']:
                fanPanel.on_button_custom()

        if key == 106:  # J key
            if len(modifier) == 0:
                self.selectPanel(jogPanel)
        if jogPanel.selected_now:
            if codepoint == '1':
                jogPanel.on_button_step_small()
            elif codepoint == '2':
                jogPanel.on_button_step_medium()
            elif codepoint == '3':
                jogPanel.on_button_step_large()
            elif codepoint == 'q':
                jogPanel.on_button_z_neg()
            elif codepoint == 'w':
                jogPanel.on_button_y_pos()
            elif codepoint == 'e':
                jogPanel.on_button_z_pos()
            elif codepoint == 'a':
                jogPanel.on_button_x_neg()
            elif codepoint == 's':
                jogPanel.on_button_y_neg()
            elif codepoint == 'd':
                jogPanel.on_button_x_pos()

        if key == 104:  # H key
            if len(modifier) == 0:
                self.selectPanel(homePanel)
            elif modifier == ['shift']:
                homePanel.on_button_all()
            elif modifier == ['ctrl']:
                homePanel.on_button_disable_steppers()
        if homePanel.selected_now:
            if codepoint == 'x':
                homePanel.on_button_x()
            elif codepoint == 'y':
                homePanel.on_button_y()
            elif codepoint == 'z':
                homePanel.on_button_z()

        else:
            print('Pressed unhandled key: {}, scancode: {}, codepoint: {}, modifier: {}'.format(
                key, scancode, codepoint, modifier
            ))


class CyanTemperanceApp(App):
    def build(self):
        return MainScreen()


if __name__ == '__main__':
    comm = PrinterComm()
    CyanTemperanceApp().run()
