import json
from serial import Serial, SerialException
from sys import stderr


# TODO: Exception handling
class PrinterComm:
    def __init__(self):
        self.config = {
            'port': '/dev/ttyUSB0',
            'baud': 115200
        }

        self.ser = Serial()
        self.is_connected = False

    def connect(self):
        try:
            self.ser = Serial(self.config['port'], self.config['baud'])
        except SerialException as ex:
            print(ex.strerror, file=stderr)
            return False

        self.is_connected = True
        return True

    def disconnect(self):
        self.ser.close()
        self.is_connected = False

    # TODO: Block until acknowledged
    def set_laser(self, pwm: int):
        assert 0 <= pwm <= 255
        if not self.is_connected:
            if not self.connect():
                return

        self.ser.write('M106P1S{};\n'.format(pwm).encode())

    # TODO: Block until acknowledged
    def set_fan(self, pwm: int):
        assert 0 <= pwm <= 255
        if not self.is_connected:
            if not self.connect():
                return

        self.ser.write('M106S{};\n'.format(pwm).encode())

    def home(self, axes: str):
        if not self.is_connected:
            if not self.connect():
                return

        if not axes:
            self.ser.write('G28;\n'.encode())

        else:
            cmd = 'G28'
            if 'X' in axes.upper():
                cmd = cmd + ' X'
            if 'Y' in axes.upper():
                cmd = cmd + ' Y'
            if 'Z' in axes.upper():
                cmd = cmd + ' Z'

            cmd = cmd + ';\n'
            self.ser.write(cmd.encode())

    def disable_steppers(self):
        if not self.is_connected:
            if not self.connect():
                return

        self.ser.write('M18;\n'.encode())

    def jog(self, axis: str, amount: float, rate: float):
        if not self.is_connected:
            if not self.connect():
                return

        cmd = 'G91;\nG0 F' + format(rate, '0.1f')
        if axis.upper() == 'X':
            cmd += ' X '
        elif axis.upper() == 'Y':
            cmd += ' Y '
        elif axis.upper() == 'Z':
            cmd += ' Z '
        else:
            # TODO: Exception
            return

        cmd += format(amount, '.1f') + ';\n'
        self.ser.write(cmd.encode())
