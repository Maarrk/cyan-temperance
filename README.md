# Cyan Temperance

Python-based GUI control for 3D printer with laser device

## Dependencies

Install with `pip`:
```
Cython
pygame
kivy
pyserial
```
